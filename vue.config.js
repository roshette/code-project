// eslint-disable-next-line import/no-extraneous-dependencies
const webpack = require('webpack');

module.exports = {
  chainWebpack: (config) => {
    const svgRule = config.module.rule('svg');

    // clear all existing loaders.
    // if you don't do this, the loader below will be appended to
    // existing loaders of the rule.
    svgRule.uses.clear();

    // add replacement loader(s)
    svgRule
      .use('vue-svg-loader')
      .loader('vue-svg-loader');
  },
  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false,
    },
  },
  configureWebpack: {
    plugins: [
      new webpack.DefinePlugin({
        'process.env.API_NETWORK': JSON.stringify(process.env.API_NETWORK),
        'process.env.API_NODE_URL': JSON.stringify(process.env.API_NODE_URL),
        'process.env.API_BACKEND_URL': JSON.stringify(process.env.API_BACKEND_URL),
        'process.env.API_EXPLORER_URL': JSON.stringify(process.env.API_EXPLORER_URL),
        'process.env.DAPP_ADDRESS': JSON.stringify(process.env.DAPP_ADDRESS),
      }),
    ],
  },
};
