import Vue from 'vue';
import device from 'current-device';
import App from './App.vue';
import AppUnsupported from './AppUnsupported.vue';
import store from './store';
import i18n from './i18n';

Vue.config.productionTip = false;

if (device.desktop()) {
  new Vue({
    store,
    i18n,
    render: h => h(App),
  }).$mount('#app');
} else {
  new Vue({
    store,
    i18n,
    render: h => h(AppUnsupported),
  }).$mount('#app');
}
