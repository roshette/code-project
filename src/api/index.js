import * as Backend from './backend';
import * as Node from './node';
import Waves from './waves';

export {
  Backend,
  Waves,
  Node,
};

export const getAvailableBalance = async (address) => {
  const { available } = await Node.getAddressBalanceInfo(
    address,
  );
  return available;
};
