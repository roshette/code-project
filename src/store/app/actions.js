import { invokeScript } from '@waves/waves-transactions';
import * as Actions from './actions-types';
import * as Mutations from './mutations-types';
import * as Api from '@/api';
import config from '@/config';
import {
  wait,
  defaultGame,
  dicesToString,
  extractResultFromString,
  Popups,
  States,
  ProcessSteps,
} from '@/utils';

export default {
  setTutorial({ dispatch, getters }, payload) {
    if (getters.isAnyPopupOpened) {
      return;
    }
    localStorage.setItem('tutorial', payload);
    dispatch(Actions.PROCEED_TUTORIAL);
  },
  setBetAmount({ commit }, payload) {
    commit(Mutations.SET_BET_AMOUNT, payload);
  },
  setPopupVisibility({ commit, getters }, { popup, visible }) {
    if (getters.isAnyPopupOpened && visible) {
      return;
    }

    if (getters.isUnavailable && !visible && Popups.INFORMATION === popup) {
      commit(Mutations.SET_POPUP_VISIBILITY, {
        popup: Popups.UNAVAILABLE,
        visible: true,
      });
      commit(Mutations.SET_POPUP_VISIBILITY, {
        popup: Popups.INFORMATION,
        visible: false,
      });
      return;
    }

    commit(Mutations.SET_POPUP_VISIBILITY, {
      popup,
      visible,
    });
  },
  setGameInformation({ commit }, payload) {
    commit(Mutations.SET_GAME_INFO_SELECTED, payload);
  },
  addDiceToSelected({ commit }, payload) {
    commit(Mutations.ADD_DICE_TO_SELECTED, payload);
  },
  removeDiceFromSelected({ commit }, payload) {
    commit(Mutations.REMOVE_DICE_FROM_SELECTED, payload);
  },
  setLoadingHandler({ commit, dispatch }) {
    window.addEventListener('load', () => {
      commit(Mutations.SET_LOADING_FINISHED);
      commit(Mutations.SET_PROCESSING_STEP, ProcessSteps.IDLE);
      commit(Mutations.SET_NEW_GAME, defaultGame());
      dispatch(Actions.INITIALIZE);
    });
  },
  async requestStatisticsUpdate({ commit }) {
    commit(Mutations.SET_LOADING_STATISTICS, true);

    const [
      statsLeaderboardCurrent,
      statsLeaderboardGeneral,
      statsTotalCurrent,
      statsTotalGeneral,
    ] = await Promise.all([
      Api.Backend.getStatsLeaderboard(config.api.gameId),
      Api.Backend.getStatsLeaderboard(),
      Api.Backend.getStatsTotal(config.api.gameId),
      Api.Backend.getStatsTotal(),
    ]);

    commit(Mutations.SET_STATS_CURRENT_LEADERBOARD, statsLeaderboardCurrent);
    commit(Mutations.SET_STATS_GENERAL_LEADERBOARD, statsLeaderboardGeneral);
    commit(Mutations.SET_STATS_CURRENT_TOTAL, statsTotalCurrent);
    commit(Mutations.SET_STATS_GENERAL_TOTAL, statsTotalGeneral);

    commit(Mutations.SET_LOADING_STATISTICS, false);
  },
  async play({ commit, getters, dispatch }) {
    // Process the game:
    try {
      // Move game to the checks state:
      commit(Mutations.SET_PROCESSING_STEP, ProcessSteps.CHECKS);

      const state = await Api.Waves.publicState();
      await dispatch(Actions.AUTHENTICATE, state);

      if (getters.playerNetwork !== config.api.network) {
        commit(Mutations.SET_POPUP_VISIBILITY, {
          popup: Popups.SWITCH_NETWORK,
          visible: true,
        });
        // Do not proceed due to wrong network:
        return;
      }

      const available = await Api.getAvailableBalance(getters.playerAddress);
      const necessary = getters.betAmount * config.wave + config.networkCosts * config.wave;
      if (available < necessary) {
        commit(Mutations.SET_POPUP_VISIBILITY, {
          popup: Popups.INSUFFICIENT_FUNDS,
          visible: true,
        });
        // Do not proceed due to insufficient funds:
        return;
      }

      // All checks passed, move to the processing:
      await dispatch(Actions.PLAY_PROCESSING);
    } catch (e) {
      switch (parseInt(e.code, 10)) {
        case 12: // Api rejected by user
          commit(Mutations.SET_POPUP_VISIBILITY, {
            popup: Popups.PERMISSIONS,
            visible: true,
          });
          break;
        case 14: // Add account
          commit(Mutations.SET_POPUP_VISIBILITY, {
            popup: Popups.ADD_ACCOUNT,
            visible: true,
          });
          break;
        default:
          console.error(e);
      }
    } finally {
      commit(Mutations.SET_PROCESSING_STEP, ProcessSteps.IDLE);
      commit(Mutations.SET_NEW_GAME, defaultGame());
    }
  },
  async [Actions.INITIALIZE]({ commit, dispatch }) {
    try {
      const WavesKeeperApi = await window.Waves.initialPromise;
      WavesKeeperApi.on(Api.Waves.eventUpdate, (state) => {
        // state might be undefined if WK was in rejected state:
        if (state) {
          dispatch(Actions.AUTHENTICATE, state);
        }
      });

      Api.Waves.install(WavesKeeperApi);
      commit(Mutations.SET_WAVES_KEEPER_INIT, true);
    } catch (e) {
      commit(Mutations.SET_WAVES_KEEPER_INIT, false);
    } finally {
      dispatch(Actions.PROCEED_TUTORIAL);
      dispatch(Actions.LIFECYCLE);
    }
  },
  async [Actions.LIFECYCLE]({ commit, dispatch, getters }) {
    try {
      const statsLastGames = await Api.Backend.getStatsLastGames(config.api.gameId);
      commit(Mutations.SET_STATS_CURRENT_LAST_GAMES, statsLastGames);
      if (getters.isUnavailable) {
        commit(Mutations.SET_GAME_UNAVAILABLE, false);
        commit(Mutations.SET_POPUP_VISIBILITY, {
          popup: Popups.UNAVAILABLE,
          visible: false,
        });
      }
    } catch (e) {
      if (!getters.isUnavailable) {
        if (getters.openedPopup) {
          commit(Mutations.SET_POPUP_VISIBILITY, {
            popup: Popups[getters.openedPopup],
            visible: false,
          });
        }
        commit(Mutations.SET_GAME_UNAVAILABLE, true);
        if (!getters.isTutorialActive) {
          commit(Mutations.SET_POPUP_VISIBILITY, {
            popup: Popups.UNAVAILABLE,
            visible: true,
          });
        }
      }
    } finally {
      await wait(config.lifecycleUpdateFrequency);
      dispatch(Actions.LIFECYCLE);
    }
  },
  async [Actions.AUTHENTICATE]({ commit }, state) {
    const {
      account: {
        address,
        publicKey,
      },
      network: {
        code,
      },
    } = state;

    commit(Mutations.SET_PLAYER_NETWORK, code);
    commit(Mutations.SET_PLAYER_ADDRESS, address);
    commit(Mutations.SET_PLAYER_PUBLICKEY, publicKey);
  },
  async [Actions.PROCEED_TUTORIAL]({ commit, getters }) {
    const active = localStorage.getItem('tutorial');
    if (active) {
      const status = JSON.parse(active);
      commit(Mutations.SET_TUTORIAL_ACTIVE, status);
      if (!status && getters.isUnavailable) {
        commit(Mutations.SET_POPUP_VISIBILITY, {
          popup: Popups.UNAVAILABLE,
          visible: true,
        });
      }
    } else {
      commit(Mutations.SET_TUTORIAL_ACTIVE, true);
    }
  },
  async [Actions.PLAY_PROCESSING]({ getters, commit }) {
    try {
      const {
        playerPublicKey,
        betAmount,
        selectedDices,
      } = getters;
      const payment = {
        amount: betAmount * config.wave + config.fees.smartAccount * config.wave,
        assetId: 'WAVES',
      };
      const func = 'bet';
      const args = [
        {
          type: 'string',
          value: dicesToString(selectedDices),
        },
      ];

      const invocation = invokeScript({
        senderPublicKey: playerPublicKey,
        dApp: config.dapp.address,
        chainId: config.api.network,
        call: {
          function: func,
          args,
        },
        payment: [payment],
        fee: 500000,
      });
      commit(Mutations.SET_CURRENT_GAME_ID, invocation.id);

      const tx = {
        type: 16,
        data: {
          ...invocation,
          fee: {
            assetId: 'WAVES',
            coins: 500000,
          },
        },
      };

      const txSigned = await Api.Waves.signTransaction(tx);

      await Api.Node.broadcastTx(txSigned);

      // Starts to poll backend for the game result:
      commit(Mutations.SET_PROCESSING_STEP, ProcessSteps.WAIT_WINNER);
      let gameWinner = null;
      do {
        try {
          // Wait one second before poll:
          // eslint-disable-next-line
          await wait(1000);
          // Get game info for current state of the match.
          // Exit from cycle if winner is defined.
          // eslint-disable-next-line
          const resp = await Api.Node.getGameResultById(invocation.id);
          if (!resp.error) {
            const value = extractResultFromString(resp.value);
            if (value === States.WON || value === States.LOST) {
              gameWinner = value;
            }
          }
        } catch (e) {
          // ignore error
        }
      } while (!gameWinner && !getters.isUnavailable);

      // Define winner:
      commit(Mutations.SET_WINNER, gameWinner);

      // Show the game result for user for 3 seconds:
      await wait(3000);
    } catch (e) {
      throw e;
    }
  },
};
