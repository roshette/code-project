/* eslint-disable */
import * as Mutations from './mutations-types';
import actions from './actions';
import config from '@/config';
import {
  coefficient,
  selectRandomDices,
  Popups,
  ProcessSteps,
} from '@/utils';

const state = {
  wavesKeeperInitialized: false,
  loaded: false,
  playerNetwork: null,
  playerAddress: null,
  playerPublicKey: null,
  processingStep: ProcessSteps.IDLE,
  loadingStatistics: false,
  gameInfoSelected: null,
  gameInfoCurrent: {
    gameId: null,
    betAmount: 2,
    selectedDices: selectRandomDices(3),
    winner: null,
  },
  unavailable: false,
  popupsVisibility: {
    [Popups.UNAVAILABLE]: config.unavailable || false,
    [Popups.INSUFFICIENT_FUNDS]: false,
    [Popups.ADD_ACCOUNT]: false,
    [Popups.PERMISSIONS]: false,
    [Popups.LEADERBOARD]: false,
    [Popups.INFORMATION]: false,
    [Popups.GAME_INFORMATION]: false,
    [Popups.SWITCH_NETWORK]: false,
    [Popups.LOOSE]: false,
    [Popups.WIN]: false,
  },
  stats: {
    current: {
      total: {
        totalPlayers: 0,
        totalCleanWonAmount: 0,
        timesPlayed: 0,
      },
      leaderboard: [],
      lastGames: [],
    },
    general: {
      total: {
        totalPlayers: 0,
        totalCleanWonAmount: 0,
        timesPlayed: 0,
      },
      leaderboard: [],
      lastGames: [],
    },
  },
  tutorialActive: false,
};

const mutations = {
  [Mutations.SET_TUTORIAL_ACTIVE](state, payload) {
    state.tutorialActive = payload;
  },
  [Mutations.SET_WAVES_KEEPER_INIT](state, payload) {
    state.wavesKeeperInitialized = payload;
  },
  [Mutations.SET_PROCESSING_STEP](state, payload) {
    state.processingStep = payload;
  },
  [Mutations.SET_LOADING_FINISHED](state) {
    state.loaded = true;
  },
  [Mutations.SET_LOADING_STATISTICS](state, payload) {
    state.loadingStatistics = payload;
  },
  [Mutations.ADD_DICE_TO_SELECTED](state, payload) {
    state.gameInfoCurrent.selectedDices.push(payload);
  },
  [Mutations.REMOVE_DICE_FROM_SELECTED](state, payload) {
    state.gameInfoCurrent.selectedDices.splice(
      state.gameInfoCurrent.selectedDices.indexOf(payload),
      1,
    );
  },
  [Mutations.SET_SELECTED_DICES](state, payload) {
    state.gameInfoCurrent.selectedDices = selectRandomDices(payload);
  },
  [Mutations.SET_BET_AMOUNT](state, payload) {
    state.gameInfoCurrent.betAmount = payload;
  },
  [Mutations.SET_WINNER](state, payload) {
    state.gameInfoCurrent.winner = payload;
  },
  [Mutations.SET_NEW_GAME](state, payload) {
    const {
      gameId,
      winner,
    } = payload;
    state.gameInfoCurrent.gameId = gameId;
    state.gameInfoCurrent.winner = winner;
  },
  [Mutations.SET_CURRENT_GAME_ID](state, payload) {
    state.gameInfoCurrent.gameId = payload;
  },
  [Mutations.SET_STATS_CURRENT_LEADERBOARD](state, payload) {
    state.stats.current.leaderboard = payload;
  },
  [Mutations.SET_STATS_CURRENT_LAST_GAMES](state, payload) {
    state.stats.current.lastGames = payload;
  },
  [Mutations.SET_STATS_CURRENT_TOTAL](state, payload) {
    const {
      totalPlayers,
      totalCleanWonAmount,
      timesPlayed,
    } = payload;
    state.stats.current.total.totalPlayers = totalPlayers || 0;
    state.stats.current.total.totalCleanWonAmount = totalCleanWonAmount || 0;
    state.stats.current.total.timesPlayed = timesPlayed || 0;
  },
  [Mutations.SET_STATS_GENERAL_LEADERBOARD](state, payload) {
    state.stats.general.leaderboard = payload;
  },
  [Mutations.SET_STATS_GENERAL_TOTAL](state, payload) {
    const {
      totalPlayers,
      totalCleanWonAmount,
      timesPlayed,
    } = payload;
    state.stats.general.total.totalPlayers = totalPlayers || 0;
    state.stats.general.total.totalCleanWonAmount = totalCleanWonAmount || 0;
    state.stats.general.total.timesPlayed = timesPlayed || 0;
  },
  [Mutations.SET_PLAYER_NETWORK](state, payload) {
    state.playerNetwork = payload;
  },
  [Mutations.SET_PLAYER_ADDRESS](state, payload) {
    state.playerAddress = payload;
  },
  [Mutations.SET_PLAYER_PUBLICKEY](state, payload) {
    state.playerPublicKey = payload;
  },
  [Mutations.SET_GAME_INFO_SELECTED](state, payload) {
    state.gameInfoSelected = payload;
  },
  [Mutations.SET_POPUP_VISIBILITY](state, { popup, visible }) {
    state.popupsVisibility[popup] = visible;
  },
  [Mutations.SET_GAME_UNAVAILABLE](state, payload) {
    state.unavailable = payload;
  },
};

const getters = {
  wavesKeeperInitialized: state => state.wavesKeeperInitialized,
  isTutorialActive: state => state.tutorialActive,
  isLoaded: state => state.loaded,
  isUnavailable: state => state.unavailable,
  isUnavailablePopup: state => state.popupsVisibility[Popups.UNAVAILABLE],
  isProcessing: state => state.processingStep > ProcessSteps.IDLE,
  isAnyPopupOpened: state => Object.entries(state.popupsVisibility)
    .some(value => value[1] === true),
  isAuthenticated: state => state.playerAddress !== null,
  processingStep: state => state.processingStep,
  selectedDices: state => state.gameInfoCurrent.selectedDices,
  betAmount: state => state.gameInfoCurrent.betAmount,
  popupsVisibility: state => state.popupsVisibility,
  loadingStatistics: state => state.loadingStatistics,
  smartAccount: state => state.gameInfoCurrent.smartAccount,
  statsCurrentTotal: state => state.stats.current.total,
  statsCurrentLeaderboard: state => state.stats.current.leaderboard,
  statsCurrentLastGames: state => state.stats.current.lastGames,
  statsGeneralTotal: state => state.stats.general.total,
  statsGeneralLeaderboard: state => state.stats.general.leaderboard,
  playerNetwork: state => state.playerNetwork,
  playerAddress: state => state.playerAddress,
  playerPublicKey: state => state.playerPublicKey,
  gameInfoSelected: state => state.gameInfoSelected,
  gameInfoCurrent: state => state.gameInfoCurrent,
  winningChance: state => {
    if (state.gameInfoCurrent.selectedDices.length > 0) {
      return ((100 / 6) * state.gameInfoCurrent.selectedDices.length);
    }

    return 0.0;
  },
  winningCoefficient: state => {
    const dices = state.gameInfoCurrent.selectedDices.length;

    return coefficient(dices);
  },
  winningAmount: state => {
    const dices = state.gameInfoCurrent.selectedDices.length;

    return state.gameInfoCurrent.betAmount * coefficient(dices);
  },
  openedPopup: state => {
    for (let key in state.popupsVisibility) {
      if(state.popupsVisibility[key] === true) {
        return key
      }
    }
    return null;
  },
};

export default {
  mutations,
  getters,
  actions,
  state,
};
