import config from '@/config';

const isFirefox = () => navigator.userAgent.toLowerCase()
  .indexOf('firefox') > -1;

const isOpera = () => navigator.userAgent.toLocaleLowerCase()
  .indexOf('opr') > -1;

export default () => {
  if (isFirefox()) {
    return config.extensions.firefox;
  }

  if (isOpera()) {
    return config.extensions.opera;
  }

  return config.extensions.chrome;
};
