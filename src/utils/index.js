import * as Api from '@/api';
import * as Popups from '@/utils/popups';
import * as States from '@/utils/states';
import * as ProcessSteps from '@/utils/steps';
import getAgentExtLink from '@/utils/agents';

export const wait = ms => new Promise(r => setTimeout(r, ms));

export const jsonHeaders = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

export const waitTxUntilAccepted = async (id) => {
  // while tx is not assigned to the BC:
  let txInBlockchain = null;
  do {
    try {
      // case #1: exception here - ERR_CONNECTION, retry
      // case #2: null object - 404, tx not in BC, retry
      // eslint-disable-next-line
      txInBlockchain = await Api.Node.getTransactionById(id);
    } catch (e) {
      // ignore error
    }
    // eslint-disable-next-line
    await wait(1000);
  } while (!txInBlockchain);
};

export const broadcastTxUntilAccepted = async (tx) => {
  let txBroadcast = null;
  do {
    try {
      // case #1: exception here - ERR_CONNECTION, retry
      // case #2: null object - 404, tx is not allowed by account-script, retry
      // case #3: not null object, but contains error, retry
      // eslint-disable-next-line
      txBroadcast = await Api.Node.broadcastTx(tx);
      if (txBroadcast && txBroadcast.error) {
        txBroadcast = null;
      }
    } catch (e) {
      // ignore error
    }
    // eslint-disable-next-line
    await wait(1000);
  } while (!txBroadcast);

  return txBroadcast;
};

export const dicesToString = dicesArray => dicesArray.join('');

export const dicesFromString = (dicesString) => {
  const dices = [];
  for (let idx = 0; idx < dicesString.length; idx += 1) {
    const dice = parseInt(dicesString.charAt(idx), 10);
    if (!dices.includes(dice) && dice >= 1 && dice <= 6 && dices.length < 5) {
      dices.push(dice);
    }
  }

  return dices;
};

export const selectRandomDices = (dicesAmount) => {
  const getRandomFromRange = (min, max) => Math.floor(min + Math.random() * (max + 1 - min));
  const dices = [];
  while (dices.length < dicesAmount) {
    const value = getRandomFromRange(1, 6);
    if (!dices.includes(value)) {
      dices.push(value);
    }
  }
  return dices.sort();
};

export const addressTruncate = (address, fist, last, placeholder) => address.slice(0, fist)
  .concat(placeholder)
  .concat(address.slice(-last));

export const extractResultFromString = (str) => {
  const values = str.split('_');
  return values[0].slice(2);
};

export const getElementAtOr = (array, index, or) => {
  if (typeof array[index] === 'undefined') {
    return or;
  }

  return array[index];
};

export const formatNumber = (number) => {
  const n = number >= 100 ? Math.trunc(number) : number;
  return new Intl.NumberFormat().format(n);
};

export const coefficient = (dices) => {
  if (dices === 1) {
    return 3.9655;
  }
  if (dices === 2) {
    return 2.4600;
  }
  if (dices === 3) {
    return 1.9000;
  }
  if (dices === 4) {
    return 1.4200;
  }

  return 1.1400;
};

export const invocationTransaction = (
  dApp,
  senderPublicKey,
  payment,
  func,
  args,
) => ({
  type: 16,
  data: {
    dApp,
    senderPublicKey,
    payment: [payment],
    call: {
      function: func,
      args,
    },
    fee: {
      assetId: 'WAVES',
      coins: 500000,
    },
  },
});

export const defaultGame = () => ({
  gameId: null,
  winner: null,
});

export { Popups };
export { States };
export { ProcessSteps };
export { getAgentExtLink };
